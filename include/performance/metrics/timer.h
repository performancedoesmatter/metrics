// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

#include <chrono>
#include <thread>
#include <performance/metrics/moving_average.h>

namespace performance {
namespace metrics {

template< typename U = std::chrono::milliseconds, bool LOG_DESTRUCTOR = false >
class StopWatch {
    std::chrono::high_resolution_clock::time_point start_;
public:
    StopWatch() {
        reset();
    }

    ~StopWatch() {
        if( LOG_DESTRUCTOR ) {
            std::cout << "duration: " << get().count() << " ms\n";
        }
    }

    void reset() {
        start_ = std::chrono::high_resolution_clock::now();
    }

    std::chrono::high_resolution_clock::time_point getStart() {
        return start_;
    }

    U get() {
        return std::chrono::duration_cast<U>(std::chrono::high_resolution_clock::now() - start_);
    }
};

template< typename GAUGE, typename TARGET, typename U = std::chrono::microseconds >
class Timer {
    U interval_;
    StopWatch<U> elapsed_;
    MovingAverage< U, 8, 16, 32 > jitter_;
    MovingAverage< U, 8, 16, 32 > diff_;
    const GAUGE& gauge_;
    TARGET& target_;

public:
    Timer( const GAUGE& gauge, TARGET& target, U interval ) : interval_( interval ), gauge_( gauge ), target_( target ) {
    }

    void run( U duration ) {
        elapsed_.reset();
        int i = 0;
        U offset = U(0);

        do {
            U now = elapsed_.get();
            U thisTarget = i++ * interval_;
            U nextTarget = thisTarget + interval_;
            U jitter = now - thisTarget;

            jitter_.push( jitter + offset );            
            offset = jitter_.fullAverage();

            target_.push( gauge_.get() );

            if( nextTarget - now > U(0) ) {
                std::this_thread::sleep_for( (nextTarget - now) - offset );
            }
        }
        while( elapsed_.get() < duration );
    }
};

} // metrics
} // performance
