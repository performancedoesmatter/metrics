// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

#include <functional>

namespace performance {
namespace metrics {

template< typename T >
class Gauge {
    std::function<T()> fn_;
public:
    typedef T value_type;

    Gauge( std::function<T()> fn) : fn_(fn) {}
    T get() const { return fn_(); }
};

} // metrics
} // performance
