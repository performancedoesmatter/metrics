// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

namespace performance {
namespace metrics {

template< typename T >
class Counter {
    T counter_;
public:
    typedef T value_type;

    Counter(T initVal = 0) : counter_(initVal) {}
    inline void inc() { counter_++; }
    inline void dec() { counter_--; }
    T get() const { return counter_; }
};

} // metrics
} // performance
