// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

#include <vector>
#include <cstdint>

namespace performance {
namespace metrics {

template< typename T >
class Histogram {
    T binStart_;
    T binWidth_;
    int numBins_;
    std::vector<std::uint64_t> bins_;
    std::uint64_t outliers_ = 0;
public:
    typedef T value_type;
    
    Histogram( T binStart, T binWidth, int numBins ) : 
        binStart_( binStart ),
        binWidth_( binWidth ),
        numBins_( numBins ),
        bins_( numBins_ ) 
    {
    }

    inline void push( T val ) {
        int bin = ( val - binStart_ ) / binWidth_;
        if( bin >= 0 && bin < numBins_ ) 
        {
            bins_[bin]++;
        }
        else
        {
            outliers_++;
        }
    }

    std::vector<std::uint64_t> get() const { return bins_; }
};

} // metrics
} // performance
