// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

#include <string>
#include <sstream>
#include <map>
#include <functional>

namespace std {
    template< typename T >
    inline std::string to_string(const std::vector<T>& vec) {
        std::stringstream ss;
        bool first = true;
        for ( auto i : vec)
        {
            if (!first)
                ss << ", ";
            ss << std::to_string(i);
            first = false;
        }
        return ss.str();
    }
}

namespace performance {
namespace metrics {

class Reporter
{
    std::map< std::string, std::function< std::string (void)> > metrics_;
    
public:
    
    template< typename T >
    void add(const std::string& name, const T& metric) {
        metrics_[name] = [&](){
            return std::to_string( metric.get() );
        };
    }

    void report() 
    {
        for (auto& r : metrics_)
        {
            std::cout << r.first << ": " << r.second() << std::endl;
        }
    }
};

} // metrics
} // performance
