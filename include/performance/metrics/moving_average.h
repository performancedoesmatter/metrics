// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

namespace performance {
namespace metrics {

#ifdef _WIN32
#include <malloc.h>
#else
#include <cstdlib>
#include <cstring>
#endif

template< typename T>
T round(float val) {
    return (T)val;
}

template<>
int round<int>(float val) {
    return (int)std::round( val );
}


template< typename T, int... X>
class MovingAverage {
public:
    typedef std::vector<T> value_type;
    void push(T val) {}
    T avg() const { return 0; }
    void get(std::vector<T>& vec) const {}
    void fullAverage( T& sum, int& count, int weight ) const {}
};

template< typename T, int N, int... M>
class MovingAverage<T, N, M... > {
    static const inline int BUFLEN = N;
    static const inline int BUFLEN_MINUS1 = BUFLEN - 1;
    static const inline float ONE_ON_BUFFLEN = 1.0f / BUFLEN;

#ifdef _WIN32
    T* buf_ = (T*)_aligned_malloc(BUFLEN * sizeof(T), 64);
#else
    T* buf_ = (T*)std::aligned_alloc(64, BUFLEN * sizeof(T));
#endif
    std::uint64_t p_ = 0;

    MovingAverage< T, M... > next_;

    T fast_avg_() const {
        T avg = T(0);
        for (int i = 0; i < BUFLEN; ++i) {
            avg += buf_[i];
        }
        return round<T>(((p_ < BUFLEN) ? avg / (float)p_ : avg * ONE_ON_BUFFLEN));
    }

public: 

    void fullAverage( T& sum, int& count, int weight = 1 ) const {
        for (int i = 0; i < BUFLEN; ++i) {
            sum += buf_[i] * weight;
        }
        if( p_ < BUFLEN ) {
            count += p_ * weight;
        }
        else {
            count += BUFLEN * weight;
        }
        next_.fullAverage( sum, count, weight * N );
    }

    typedef std::vector<T> value_type;

    MovingAverage() {
        memset(buf_, 0, BUFLEN * sizeof(T));
    }

    ~MovingAverage() {
#ifdef _WIN32
        _aligned_free(buf_);
#else
        std::free(buf_);
#endif
    }

    inline std::uint64_t idx(std::uint64_t i) {
        return i & BUFLEN_MINUS1;
    }

    inline void push(T val) {
        auto i = idx(p_++);
        buf_[i] = val;
        if (i == BUFLEN_MINUS1) {
            p_ = BUFLEN;  // reset p to BUFLEN to avoid overflow - don't reset to 0 as a non-full buffer is a special case
            next_.push(fast_avg_());
        }
    }

    T avg() const {
        if (p_) {
            return fast_avg_();
        }
        return T(0);
    }

    T fullAverage() const {
        T sum = T(0);
        int count = 0;
        fullAverage( sum, count );
        if( count == 0 ) return T(0);
        return sum / count;
    }


    void get(std::vector<T>& vec) const {
        vec.emplace_back(avg());
        next_.get(vec);
    }

    std::vector<T> get() const 
    { 
        std::vector<T> vec;
        get(vec);
        return vec; 
    }
};

} // metrics
} // performance
