// Copyright (c) Performance Does Matter. All Rights Reserved.

#include <iostream>
#include <random>
#include <vector>
#include "performance/metrics/metrics.h"

template< typename T >
void createRandomData( std::vector<T>& data, T min, T max)
{
    performance::metrics::StopWatch< std::chrono::milliseconds, true > _;
    std::cout << "Creating random data..." << std::endl;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<> dis(min, max);

    std::generate(data.begin(), data.end(), std::bind(dis, std::ref(mt)));
}

int main() {
    const int COUNT = 1000*1000;
    std::vector<int> data(COUNT);

    createRandomData(data, 0, 100);

    const std::int64_t N = 256;

    performance::metrics::Reporter metrics;
    performance::metrics::Counter<int> c1;
    performance::metrics::Counter<int> c2;
    performance::metrics::MovingAverage<int, N, N, N, N> ma;
    performance::metrics::MovingAverage<float, N, N, N, N> ma2;
    performance::metrics::Histogram<int> h1( 0, 20, 5 );
    performance::metrics::Histogram<float> h2( 0.0f, 20.0f, 5 );

    metrics.add("Counter 1", c1);
    metrics.add("Counter 2", c2);
    metrics.add("Moving Average (int)", ma);
    metrics.add("Moving Average (float)", ma2);
    metrics.add("Histogram (int)", h1);
    metrics.add("Histogram (float)", h2);

    for (int i = 0; i < 100; ++i) { c1.inc(); }
    for (int i = 0; i < 200; ++i) { c2.inc(); }

    std::cout << "Streaming data to moving average calculator (int)..." << std::endl;
    {
        performance::metrics::StopWatch< std::chrono::milliseconds, true > _;
        for (int i : data) {
            ma.push(i);
        }
    }

    std::cout << "Streaming data to moving average calculator (float)..." << std::endl;
    {
        performance::metrics::StopWatch< std::chrono::milliseconds, true > _;
        for (int i : data) {
            ma2.push(i);
        }
    }

    std::cout << "Streaming data to histogram... (int)" << std::endl;
    {
        performance::metrics::StopWatch< std::chrono::milliseconds, true > _;
        for (int i : data) {
            h1.push(i);
        }
    }

    std::cout << "Streaming data to histogram... (float)" << std::endl;
    {
        performance::metrics::StopWatch< std::chrono::milliseconds, true > _;
        for (int i : data) {
            h2.push(i);
        }
    }

    metrics.report();


    performance::metrics::Reporter metrics2;

    int c = 123;
    performance::metrics::Gauge<int> g1( [&]() { return c; } );
    performance::metrics::MovingAverage<int, N, N, N, N> gma1;

    performance::metrics::Timer timer( g1, gma1, std::chrono::milliseconds( 10 ) );
    timer.run( std::chrono::seconds( 5 ) );

    metrics2.add( "Gague moving average", gma1 );
    metrics2.report();
}
